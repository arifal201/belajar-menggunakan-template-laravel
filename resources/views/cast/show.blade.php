@extends('templates.master')
@section('content')
	<h2>Show Cast {{$cast->id}}</h2>
	<h4>{{$cast->name}}</h4>
	<p>{{$cast->age}}</p>
	<p>{{$cast->bio}}</p>
	<p>{{$cast->created_at}}</p>
	<p>{{$cast->updated_at}}</p>
@endsection