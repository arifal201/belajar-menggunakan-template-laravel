@extends('templates.master')
@section('content')    
	<div>
		<h2>Tambah Data</h2>
			<form action="/cast" method="POST">
				@csrf
				<div class="form-group">
					<label for="name">Name</label>
					<input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Name">
					@error('name')
						<div class="alert alert-danger">
							{{ $message }}
						</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="age">Age</label>
					<input type="text" class="form-control" name="age" id="age" placeholder="Masukkan Age">
					@error('age')
						<div class="alert alert-danger">
							{{ $message }}
						</div>
					@enderror
				</div>
				<div class="form-group">
					<label for="bio">Bio</label>
					<input type="text" class="form-control" name="bio" id="bio" placeholder="Masukkan Bio">
					@error('bio')
						<div class="alert alert-danger">
							{{ $message }}
						</div>
					@enderror
				</div>
				<button type="submit" class="btn btn-primary">Tambah</button>
			</form>
	</div>
@endsection