@extends('templates.master')
@section('content')    
  <div>
    <h2>Edit Cast {{$cast->id}}</h2>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" name="name" value="{{$cast->name}}" id="name" placeholder="Masukkan Name">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="age">age</label>
            <input type="text" class="form-control" name="age"  value="{{$cast->age}}"  id="age" placeholder="Masukkan Age">
            @error('age')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
          <label for="bio">bio</label>
          <input type="text" class="form-control" name="bio"  value="{{$cast->bio}}"  id="bio" placeholder="Masukkan Bio">
          @error('bio')
              <div class="alert alert-danger">
                  {{ $message }}
              </div>
          @enderror
      </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
  </div>
@endsection