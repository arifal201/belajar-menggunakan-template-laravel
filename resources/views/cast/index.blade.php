@extends('templates.master')
@section('content')
<a href="{{route('cast.create')}}" class="btn btn-primary">Tambah</a>
<table class="table">
	<thead class="thead-light">
		<tr>
			<th scope="col">id</th>
			<th scope="col">name</th>
			<th scope="col">age</th>
			<th scope="col">bio</th>
			<th scope="col">created_at</th>
			<th scope="col">updated_at</th>
			<th scope="col">Actions</th>
		</tr>
	</thead>
	<tbody>
		@forelse ($cast as $key=>$value)
			<tr>
				<td>{{$value->id}}</th>
				<td>{{$value->name}}</td>
				<td>{{$value->age}}</td>
				<td>{{$value->bio}}</td>
				<td>{{$value->created_at}}</td>
				<td>{{$value->updated_at}}</td>
				<td>
					<a href="{{route('cast.show',['cast'=> $value->id])}}" class="btn btn-info">Show</a>
					<a href="{{ route('cast.edit',['cast'=> $value->id])}}" class="btn btn-primary">Edit</a>
					<form action="{{ route('cast.destroy',['cast' => $value->id]) }}" method="POST">
						@csrf
						@method('DELETE')
						<input type="submit" class="btn btn-danger my-1" value="Delete">
					</form>
				</td>
			</tr>
		@empty
			<tr colspan="3">
				<td>No data</td>
			</tr>  
		@endforelse              
	</tbody>
</table>
@endsection
